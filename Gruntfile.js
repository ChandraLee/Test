module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);
  var config = {
    appUrl: 'app',
    templateUrl: 'app/source',
    pubUrl: 'dist'
  };
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    config: config,
    connect: {
      //这里为插件子刷新方式
      options: {
        port: 9000,
        hostname: 'localhost',
        livereload: 35729
      },
      server: {
        options: {
          open: true,
          base: [
            '.'
          ]
        }
      }
    },
    concat: {
      js: {
        expand: true,
        cwd: '<%= config.appUrl %>/js/',
        src: '{,*/}*.js',
        dest: '<%= config.pubUrl %>/js/'
      },
      css: {
        expand: true,
        cwd: '<%= config.appUrl %>/css/',
        src: '{,*/}*.css',
        dest: '<%= config.pubUrl %>/css/'
      }
    },
    replace: {
      dist: {
        options: {
          patterns: [{
            match: /href="..\/css\//g,
            replacement: 'href="../../public/css/'
          }, {
            match: /src="..\/js\//g,
            replacement: 'src="../../public/js/'
          }]
        },
        files: [{
          expand: true,
          src: ['dist/html/*.html']
        }]
      }
    },
    jade: {
      compile: {
        options: {
          data: {
            debug: false
          },
          pretty: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.templateUrl %>/jade/',
          src: '{,*/}*.jade',
          dest: '<%= config.appUrl %>/html',
          ext: ".html"
        }]
      }
    },
    jshint: {
      options: {
        strict: false,
        laxbreak: true,
        debug: true,
        globals: {
          angular: true,
          $: true,
          _: true
        }
      },
      all: ['<%= config.appUrl %>/js/{,*/}*.js', '<%= config.pubUrl %>/js/{,*/}*.js']
    },
    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          // preserveLineBreaks: true,
          collapseWhitespace: true,
          removeCommentsFromCDATA: true,
          useShortDoctype: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.appUrl %>/html',
          src: '{,*/}*.html',
          dest: '<%= config.pubUrl %>/html'
        }]
      }
    },
    rev: {
      options: {
        encoding: 'utf8',
        algorithm: 'md5',
        length: 8
      },
      images_fonts: {
        files: [{
          src: [
            'dist/images/{,*/}*.{jpg,jpeg,gif,png,ico}'
          ]
        }]
      },
      css_js: {
        files: [{
          src: [
            'dist/{,*/}*.{js,css}'
          ]
        }]
      }
    },
    usemin: {
      css: ['dist/css/{,*/}*.css'],
      js: 'dist/js/{,*/}*.js',
      html: 'dist/html/{,*/}*.html',
      options: {
        assetsDirs: ['dist/js/', 'dist/css/'],
        patterns: {
          js: [
            [/(\/images\/[\/\w-]+\.png)/, 'replace image in js']
          ]
        }
      }
    },
    useminPrepare: {
      options: {
        dest: '<%= config.pubUrl %>/html'
      },
      html: '<%= config.appUrl %>/{,**/}*.html'
        //  html: '<%= config.app %>/index.html'
    },
    uglify: {
      options: {
        footer: '\n/*! 最后修改于： <%= grunt.template.today("yyyy-mm-dd") %> */', //添加footer
        mangle: false
      },
      target: {
        expand: true,
        cwd: '<%= config.appUrl %>/js/',
        src: '{,*/}*.js',
        dest: '<%= config.pubUrl %>/js/',
        ext: '.min.js'
      }
    },
    watch: {
      sass: {
        files: ['<%= config.templateUrl %>/sass/*.{scss,sass}'],
        tasks: ['compass:dist']
      },
      livereload: {
        options: {
          livereload: '<%=connect.options.livereload%>'
        },
        files: [
          '<%= config.appUrl %>/html/{,*/}*.html',
          '<%= config.appUrl %>/css/{,*/}*.css',
          '<%= config.appUrl %>/js/{,*/}*.js',
          '<%= config.appUrl %>/images/{,*/}*.{png,jpg,gif,jpeg,ico}',
          '<%= config.pubUrl %>/html/{,*/}*.html',
          '<%= config.pubUrl %>/css/{,*/}*.css',
          '<%= config.pubUrl %>/js/{,*/}*.js',
          '<%= config.pubUrl %>/images/{,*/}*.{png,jpg,gif,jpeg,ico}',
        ]
      }
    },
    cssmin: {
      compile: {
        files: [{
          expand: true,
          cwd: '<%= config.appUrl %>/css/',
          src: '{,*/}*.css',
          dest: "<%= config.pubUrl %>/css/",
          ext: '.min.css'
        }]
      }
    },
    clean: {
      init: {
        options: {
          force: true
        },
        src: ["<%= config.pubUrl %>/*"]
      }
    },
    copy: {
      main: {
        nonull: true,
        files: [
          // includes files within path
          {
            expand: true,
            cwd: '<%= config.appUrl %>/html',
            src: '{,*/}*.html',
            dest: '<%= config.pubUrl %>/html'
          }, {
            expand: true,
            cwd: '<%= config.appUrl %>/images',
            src: '{,*/}*.{png,jpg,gif,jpeg}',
            dest: '<%= config.pubUrl %>/images'
          }
        ]
      }
    },
    compass: {
      dist: {
        options: {
          config: 'config.rb'
        }
      }
    },
    //   uglify: {
    //    generated: {
    //     files: [
    //       {
    //          cwd: '.tmp/concat/js',
    //           src: '{,*/}*.js',
    //           dest: '<%= config.pubUrl %>/js'
    //       }
    //     ]
    //   },
    //   concat: {
    //    generated: {
    //     files: [
    //       {
    //          cwd: '.tmp/concat/js',
    //           src: '{,*/}*.js',
    //           dest: '<%= config.pubUrl %>/js'
    //       }
    //     ]
    //   }
    // }
  });
  grunt.registerTask('live', ['compass:dist', 'connect:server', 'jshint', 'jade', 'htmlmin', 'watch']);
  grunt.registerTask('build', ["clean", 'jshint', 'compass:dist', 'jade', 'copy', 'useminPrepare', 'concat:generated', 'uglify:generated', 'cssmin:generated', 'rev', 'usemin', 'htmlmin']);
  // grunt.registerTask('build', ["clean", 'jade', 'copy','useminPrepare','concat:generated','uglify:generated','cssmin','rev','usemin']);



  // grunt.registerTask('build', ["clean", 'useminPrepare','jade', 'htmlmin', 'cssmin', 'uglify', 'copy','rev','usemin']);
};