// var myApp = angular.module('myApp',['ngRoute','dockerServices']);
// myApp.config(function($routeProvider) {
// 	$routeProvider.when('/1',{
// 		templateUrl:'1.html',
// 		controller:"firstCtr"
// 	}).when('/2',{
// 		templateUrl:"2.html",
// 		controller:"seconedCtr"
// 	}).otherwise({
// 		redirectTo:'/1'
// 	});
// });
var myApp = angular.module('myApp',['ui.router','dockerServices']);
myApp.config(function($stateProvider,$urlRouterProvider) {
	$urlRouterProvider.when("",'home.1');
	$stateProvider.state("home", {
		url:"/page",
            templateUrl: "page.html"
        })
        .state("home.1", {
        	url:"/1",
            templateUrl: "1.html"
        })
         .state("home.2", {
        	url:"/2",
            templateUrl: "2.html"
        })
        .state("about", {
            templateUrl: "2.html"
        });
	// .state("home",{
	//  	views:{
	//  		"":{
	//  			template:"<h1>home</h1>"
	//  		},
	//  		"1":{
	//  			template:"<h1>111</h1>"
	//  		}
	//  	}
	//  }).state("about",{
	//  	template:"<h1>about</h1>"
	//  });

    
	// $stateProvider.state('page',{
	// 	url:
	// }).state('page.1',{
	// 	templateUrl:'1.html',
	// 	controller:"firstCtr"
	// }).state('2',{
	// 	templateUrl:"2.html",
	// 	controller:"seconedCtr"
	// });
});