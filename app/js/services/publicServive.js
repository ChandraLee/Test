var dockerServices = angular.module('dockerServices', []);
dockerServices.service('CONSTANT', function() {
	return {
		USER_STATUS: {
			//在线
			CHAT: "online",
			//下线
			OFFLINE: "offline",
			//忙碌
			DND: "dnd",
			//离开
			AWAY: "away",
			//长时间离开
			XA: "xa"
		},
		USERNAME: "CHANDRALEE"
	};
});
dockerServices.service('$utilService', function() {
	function isEmptyObject(obj) {
		for (var n in obj) {
			if (obj.hasOwnProperty(n)) {
				return false;
			}
		}
		return true;
	}
	return {
		isEmptyObject: isEmptyObject
	};

});