myApp.directive('hello', ['$rootScope', function($rootScope){
return {
		// scope: {}, // {} = isolate, true = child, false/undefined = no change
		// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
		restrict: 'AE', // E = Element, A = Attribute, C = Class, M = Comment
		template: '<button ng-click="toggle()" class="tog">i am {{name}}，hello！</button>',
		// templateUrl: '',
		replace: true,
		// transclude: true,
		link: function($scope, iElm, iAttrs, controller) {
			$scope.name="chandra";
			// $scope.toggle=function(){
			// 	$rootScope.small = !$rootScope.small ;
			// };
		}
	};
}]);
myApp.directive("scroll", function ($window,$rootScope) {
    return function(scope, element, attrs) {
        angular.element($window).bind("scroll", function() {
            // console.log($('body').scrollTop() );
            if($('body').scrollTop() < 50){
            	$rootScope.small = false;
            	
            }
            else{
            	$rootScope.small= true;
            }
            scope.$apply();
        });
    };
});